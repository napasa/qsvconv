#pragma once
#pragma once
#include <windows.h>

#include <string>
#if defined(_WIN32) && !defined(WIN32)
    #define WIN32
#endif




class File {
public:
    enum FileType {
        PDF,
        DOC,
        DOCX,
        RTF,
        XLS,
        XLSX,
        PPT,
        PPTX,
        TXT,
        PNG,
        JPG,
        BMP,
        ICON,
        GIF,
        Exif,
        TIFF,
        WMF,
        EMF,
        DIR,
        HTML,
        WEBP
    };
public:
    File() {}
    File(const std::wstring& path) : path(path) {}
    File(const File& file) : path(file.path) { }
    File(const File&& file) : path(std::move(file.path)) {}
    File& operator=(const File& file);
    const std::wstring& Path() const;
    void Path(const std::wstring& val);
    std::wstring Extension()const;
    bool Exist();
    long BytesSize();
    virtual ~File() {}
    std::wstring Name()const;
    std::wstring Basename()const;
    void Rename(const std::wstring& name);
    static std::wstring Basename(const std::wstring& filename);
    static std::wstring Name(const std::wstring& filename);
    static std::wstring Directory(const std::wstring& filename = std::wstring());
    static std::wstring FullPath();
    static std::wstring Extension(const std::wstring& filename);
    static std::wstring TempPath();
    static std::wstring GenerateUniquePath(std::wstring& originPath, bool isFile = true);
    #ifdef WIN32
    static  HINSTANCE OpenFileBySystem(const std::wstring& filename);
    static  std::wstring ChooseDirectory(HWND hwnd);
    static  bool OpenDirAndSelectFile(const std::wstring& pathname);
    static  std::wstring SelectMultiFiles(HWND hwnd, const std::wstring& title, LPCTSTR pFilter, int filterIndex = 1);
    static  bool CreateDeepDirectory(const std::wstring& path);
    static  bool Exist(const std::wstring& path);
    static  int CountFileInDir(const std::wstring& path);
    static  long BytesSize(const std::wstring& path);
    static  std::wstring FileTypeToString(File::FileType fileType);
    static  bool Copy(const File& file, const std::wstring destination, bool cut);
    static  bool Delete(const File& file);
    static const std::wstring DefaultPathDir();
    #endif
protected:
    std::wstring path;
};
