#pragma once
#include <string>
#include <fstream>
#include "FlvWriter.h"

class TransCoder {
public:
    TransCoder(std::wstring _qsvPath, std::wstring _outputPath);
    void Transcode();
    ~TransCoder();
    int ProgressInterval() const { return progressInterval; }
    int Index() const { return index; }
    HWND Dlg() const { return hDlg; }
    int Msg() const { return msg; }
    void TransInfo(HWND hDlg, int msg, int progressInterval, int index) {
        this->hDlg = hDlg;
        this->msg = msg;
        this->progressInterval = progressInterval;
        this->index = index;
    }
    std::wstring TempFilePath() const { return tempFilePath; }
    void TempFilePath(std::wstring val) { tempFilePath = val; }
private:
    bool CheckTAG();
    bool CheckVersion();
    void SeekBegin();
    void SkipMeta();
    void SeekNextTag();
    std::pair<Byte*, int> GetTag();
    bool ReadNext();
    int BytesToInt(char* paramArrayOfByte, int paramInt);
    long BytesToLong(char* paramArrayOfByte, int paramInt);
private:
    std::wstring qsvPath;
    std::wstring outputPath;
    std::wstring outputName;
    std::ifstream qsv;
    std::ofstream temp;
    std::wstring tempFilePath;
    HWND hDlg;
    int msg;
    int progressInterval;
    int index;
};

