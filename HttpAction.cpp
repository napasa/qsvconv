#include "pch.h"
#include "HttpAction.h"
#include "Utf8.h"

HttpAction* HttpAction::GetInstance() {
    static HttpAction userAction;
    return &userAction;
}

void HttpAction::Do(const Http::Task& task) {
    UserData* userdata = reinterpret_cast<UserData*>(task.UserData());
    if (userdata == nullptr) {
        return;
    }
    if (task.CurlCode() == CURLE_OK) {
        WindowData* windowData = reinterpret_cast<WindowData*>(userdata);
        if (userdata->requestType == RequestType::SOFTWARE_STRATEGY
                || userdata->requestType == RequestType::QUERY_STATUS
                || userdata->requestType == RequestType::GET_QRCODE) {
            ::PostMessage(windowData->hwnd, windowData->wndMSG, WPARAM(new Http::Response(task)), windowData->lparam);
        } else {
            ::PostMessage(windowData->hwnd, WindowData::WN_INVALID_REQUEST, NULL, NULL);
        }
    }
}

int HttpAction::Progress(double totaltime, double dltotal, double dlnow, double ultotal, double ulnow, const Http::Task& task) {
    return 0;
}

void HttpAction::GetConfig(HWND hwnd, int msg) {
    Http::URL url("https://gitee.com/napasa/qsvconv/raw/master/cfg");
    ROUTER.Get(url, HTTP_ACTION_OBJ, new WindowData(RequestType::SOFTWARE_STRATEGY, hwnd, msg));
}


void HttpAction::GetQrcode(HWND hwnd, int msg, std::string url) {
    Http::URL uRL(url);
    ROUTER.Get(uRL, HTTP_ACTION_OBJ, new WindowData(RequestType::GET_QRCODE, hwnd, msg));
}

void HttpAction::QueryPayStatus(HWND hwnd, int msg, std::string url, std::string orderid) {
    Http::URL uRL(url + "?out_trade_no=" + orderid);
    ROUTER.Get(uRL, HTTP_ACTION_OBJ, new WindowData(RequestType::QUERY_STATUS, hwnd, msg));
}

HttpAction::HttpAction() {
}


HttpAction::~HttpAction() {
}

bool HttpAction::ParseResponse(Json::Value& value, WPARAM res, UINT uMsg, HWND hwnd, std::wstring& msg) {
    Http::Response* response = reinterpret_cast<Http::Response*>(res);
    if (response->CurlCode() == CURLE_OK) {
        Json::Reader reader;
        if (reader.parse(response->MemoryAddr(), response->MemoryAddr() + response->Size(), value)) {
            if (value["code"] == 0) {
                return true;
            } else {
                Utf8 prompt(value["msg"].asString(), true);
                msg = Utf8::Utf8ToUtf16(prompt).c_str();
            }
        } else {
            msg = L"服务器异常";
        }
    } else {
        msg = L"本地网络错误";
    }
    delete response;
    return false;
}

int WindowData::WN_INVALID_REQUEST = WM_INVALID_REQUEST;
