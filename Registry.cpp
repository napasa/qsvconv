﻿// Registry.cpp : implementation file
//

#include "pch.h"
#include "Registry.h"
/////////////////////////////////////////////////////////////////////////////
// CRegistry

Registry::Registry(HKEY hKey) : m_hKey(hKey) {

}

Registry::~Registry() {
    Close();
}

/////////////////////////////////////////////////////////////////////////////
// CRegistry Functions

BOOL Registry::CreateKey(LPCTSTR lpSubKey) {


    HKEY hKey;
    DWORD dw;
    long lReturn = RegCreateKeyEx(m_hKey, lpSubKey, 0L, NULL, REG_OPTION_VOLATILE, KEY_ALL_ACCESS | KEY_WOW64_64KEY, NULL, &hKey, &dw);

    if (lReturn == ERROR_SUCCESS) {
        m_hKey = hKey;
        return TRUE;
    }

    return FALSE;

}

LONG Registry::Open(LPCTSTR lpSubKey, REGSAM samDesired) {
    HKEY hKey;
    long lReturn = RegOpenKeyEx(m_hKey, lpSubKey, 0L, samDesired, &hKey);

    if (lReturn == ERROR_SUCCESS) {
        m_hKey = hKey;
    }
    return lReturn;
}

void Registry::Close() {
    if (m_hKey) {
        RegCloseKey(m_hKey);
        m_hKey = NULL;
    }

}

BOOL Registry::DeleteValue(LPCTSTR lpValueName) {

    long lReturn = RegDeleteValue(m_hKey, lpValueName);

    if (lReturn == ERROR_SUCCESS) {
        return TRUE;
    }
    return FALSE;

}

BOOL Registry::DeleteKey(HKEY hKey, LPCTSTR lpSubKey) {
    long lReturn = RegDeleteValue(hKey, lpSubKey);

    if (lReturn == ERROR_SUCCESS) {
        return TRUE;
    }
    return FALSE;

}

BOOL Registry::Write(LPCTSTR lpSubKey, DWORD dwVal) {
    long lReturn = RegSetValueEx(m_hKey, lpSubKey, 0L, REG_DWORD, (const BYTE*)&dwVal, sizeof(DWORD));

    if (lReturn == ERROR_SUCCESS) {
        return TRUE;
    }

    return FALSE;

}

BOOL Registry::Write(LPCTSTR lpSubKey, __int64 dwVal) {
    long lReturn = RegSetValueEx(m_hKey, lpSubKey, 0L, REG_QWORD, (const BYTE*)&dwVal, sizeof(__int64));

    if (lReturn == ERROR_SUCCESS) {
        return TRUE;
    }

    return FALSE;
}


BOOL Registry::Write(LPCTSTR lpValueName, LPCTSTR lpValue) {
    int len = wcslen(lpValue) * sizeof(TCHAR);
    long lReturn = RegSetValueEx(m_hKey, lpValueName, 0L, REG_SZ, (const BYTE*)lpValue, len + 1);

    if (lReturn == ERROR_SUCCESS) {
        return TRUE;
    }

    return FALSE;

}

BOOL Registry::Read(LPCTSTR lpValueName, DWORD* pdwVal) {
    DWORD dwType;
    DWORD dwSize = sizeof(DWORD);
    long lReturn = RegQueryValueEx(m_hKey, lpValueName, NULL, &dwType, (BYTE*)pdwVal, &dwSize);

    if (lReturn == ERROR_SUCCESS) {
        return TRUE;
    }
    return FALSE;

}

BOOL Registry::Read(LPCTSTR lpValueName, __int64* pqwVal) {
    DWORD dwType;
    DWORD dwSize = sizeof(__int64);
    long lReturn = RegQueryValueEx(m_hKey, lpValueName, NULL, &dwType, (BYTE*)pqwVal, &dwSize);

    if (lReturn == ERROR_SUCCESS) {
        return TRUE;
    }
    return FALSE;
}

BOOL Registry::RestoreKey(LPCTSTR lpFileName) {
    long lReturn = RegRestoreKey(m_hKey, lpFileName, REG_WHOLE_HIVE_VOLATILE);
    if (lReturn == ERROR_SUCCESS) {
        return TRUE;
    }

    return FALSE;
}

BOOL Registry::SaveKey(LPCTSTR lpFileName) {
    long lReturn = RegSaveKey(m_hKey, lpFileName, NULL);
    if (lReturn == ERROR_SUCCESS) {
        return TRUE;
    }

    return FALSE;
}

BOOL Registry::Read(LPCTSTR lpValueName, TCHAR* pVal, DWORD* lpSize) {


    DWORD dwType;
    long lReturn = RegQueryValueEx(m_hKey, lpValueName, NULL, &dwType, (BYTE*)pVal, lpSize);

    if (lReturn == ERROR_SUCCESS) {
        return TRUE;
    }
    return FALSE;

}