﻿#pragma once
#include <Windows.h>

class Registry {
public:
    Registry(HKEY hKey = HKEY_LOCAL_MACHINE);

public:
    BOOL SaveKey(LPCTSTR lpFileName);
    BOOL RestoreKey(LPCTSTR lpFileName);
    BOOL Read(LPCTSTR lpValueName, TCHAR* pVal, DWORD* lpSize);
    BOOL Read(LPCTSTR lpValueName, DWORD* pdwVal);
    BOOL Read(LPCTSTR lpValueName, __int64* pqwVal);
    BOOL Write(LPCTSTR lpSubKey, LPCTSTR lpVal);
    BOOL Write(LPCTSTR lpSubKey, DWORD dwVal);
    BOOL Write(LPCTSTR lpSubKey, __int64 dwVal);
    BOOL DeleteKey(HKEY hKey, LPCTSTR lpSubKey);
    BOOL DeleteValue(LPCTSTR lpValueName);
    void Close();
    LONG Open(LPCTSTR lpSubKey, REGSAM samDesired);
    BOOL CreateKey(LPCTSTR lpSubKey);
    virtual ~Registry();

protected:
    HKEY m_hKey;

};

