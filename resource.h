//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by QSV2FLV_CPP.rc
//
#define IDB_ADDFILE                     1000
#define IDB_CLEARLIST                   1001
#define IDB_STARTCONVERT                1002
#define IDB_STOPCONVERT                 1003
#define IDB_ABOUT                       1004
#define IDB_DESCRIPTION                 1005
#define IDBL_ITEMS                      1006
#define IDC_MFCEDITBROWSE1              1007
#define IDC_DIRECTORY                   1008
#define IDC_BROWSEDIR                   1009
//#define IDC_MONEY                       1010
#define IDC_PROMPTION                   1011
#define IDC_OPEN                        1012

// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
    #ifndef APSTUDIO_READONLY_SYMBOLS
        #define _APS_NEXT_RESOURCE_VALUE        108
        #define _APS_NEXT_COMMAND_VALUE         40001
        #define _APS_NEXT_CONTROL_VALUE         1013
        #define _APS_NEXT_SYMED_VALUE           101
    #endif
#endif
