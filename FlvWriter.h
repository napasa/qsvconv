#pragma once
#include <string>
#include <fstream>
#include "Metadata.h"
#include <windows.h>

class FlvWriter {
public:
    FlvWriter(std::wstring outputPath, std::wstring sourcePath);
    ~FlvWriter();
    std::wstring FullName();
    std::ofstream& BaseStream();
    void WriteTag(void* tagPoint, int size);
    void Parse();
    void Output();
    static std::streamsize GetStreamLength(std::ifstream&  fs);
    static std::streamsize GetStreamLength(std::ofstream&  fs);
    int ProgressInterval() const { return progressInterval; };
    int Index() const { return index; };
    HWND Dlg() const { return hDlg; };
    int Msg() const { return msg; };
    void TransInfo(HWND hDlg, int msg, int progressInterval, int index) {
        this->hDlg = hDlg;
        this->msg = msg;
        this->progressInterval = progressInterval;
        this->index = index;
    }
private:
    std::streampos Position();
    void Position(std::streampos position);
    void SeekFirstTag();
    void SeekNextTag();
    void SeekLastTag();
    bool IsNotEnd();
    std::vector<int> ParseAudioTag();
    std::vector<int> ParseVideoTag();
    bool IsKeyFrame();
    int GetTimeStamp();
    int GetTagType();
    std::vector<unsigned char> IntToBytes(int num);
    std::vector<unsigned char> ShorToBytes(int num);
    std::vector<unsigned char> DoubleToBytes(double num);
    std::vector< unsigned char> StringToBytes(std::wstring str);
    std::vector< unsigned char> BoolToBytes(bool bl);
    std::ofstream flv;
    std::ifstream source;
    Metadata meta;
    std::wstring path;
    HWND hDlg;
    int msg;
    int progressInterval;
    int index;
};

