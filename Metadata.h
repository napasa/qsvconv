#pragma once
#include <list>
#include <vector>
typedef char Byte;
class Metadata {
public:
    class Keyframes {
    public:
        std::list<std::streampos> filePositions;
        std::list<double> times;
        void Add(std::streampos pos, double t) {
            filePositions.push_back(pos);
            times.push_back(t);
        }
        int Count() {
            return filePositions.size();
        }
    };
    enum AMFType {
        Number = 0,
        Boolean = 1,
        String = 2,
        Object = 3,
        MovieClip = 4,
        Null = 5,
        Undefined = 6,
        Reference = 7,
        ECMAArray = 8,
        StrictArray = 10,
        Date = 11,
        LongString = 12
    };
    class FlvMetaItem {
    public:
        FlvMetaItem(std::wstring key, AMFType type, int value):
            key(key), type(type), intValue(value) {}
        FlvMetaItem(std::wstring key, AMFType type, double value) :
            key(key), type(type), doubleValue(value) {}
        FlvMetaItem(std::wstring key, AMFType type, bool value) :
            key(key), type(type), boolValue(value) {}
        FlvMetaItem(std::wstring key, AMFType type, std::wstring value) :
            key(key), type(type), strValue(value) {}
        std::wstring key;
        AMFType type;
        int intValue;
        double doubleValue;
        bool boolValue;
        std::wstring strValue;
    };
    static const int MetaDataSizePosition = 0xE;
    static const int DurationPosition = 0x35;
    static const int WidthPosition = 0x45;
    static const int HeightPosition = 0x56;
    static const int VideoDataRatePosition = 0x6E;
    static const int FrameRatePosition = 0x82;
    static const int VideoCodeCIDPosition = 0x99;
    static const int AudioSampleRatePosition = 0xB3;
    static const int AudioSampleSizePosition = 0xCD;
    static const int StereoPosition = 0xDE;
    static const int AudioCodeCIDPosition = 0xEE;
    static const int FileSizePosition = 0x101;
    static const int LastTimeStampPosition = 0x119;
    static const int LastKeyFrameTimeStampPosition = 0x139;
    static const int AudioDelayPosition = 0x14E;
    static const int CanSeekToEndPosition = 0x165;
    static const int CreationDatePosition = 0x177;
    static const int MetaDataCreatorPosition = 0x19A;

    static Byte Meta_1[410];
    static Byte Meta_2[28];
    static Byte Meta_3[8];
    static Byte Meta_4[6];

public:
    Keyframes keyframes;
    std::vector<FlvMetaItem> metadaArray;
    Metadata();
    ~Metadata();

    double Duration();
    void Duration(double duration);

    int Width();
    void Width(int width);

    int Height();
    void Height(int height);

    int VideoDataRate();
    void VideoDataRate(int videoDetaRate);

    double FrameRate();
    void FrameRate(double frameRate);

    int VideoCodeCID();
    void VideoCodeCID(int videoCodeCID);

    int AudioSampleRate();
    void AudioSampleRate(int audioSampleRate);

    int AudioSampleSize();
    void AudioSampleSize(int audioSampleSize);

    bool Stereo();
    void Stereo(bool stereo);

    int AudioCodeCID();
    void AudioCodeCID(int audioCodeCID);

    int FileSize();
    void FileSize(int fileSize);

    double LastTimesStamp();
    void LastTimesStamp(double lastTimesStamp);

    double LastKeyFrameTimeStamp();
    void LastKeyFrameTimeStamp(double lastKeyFrameTimeStamp);

    int AudioDelay();
    void AudioDelay(int audioDelay);

    bool CanSeekToEnd();
    void CanSeekToEnd(bool canSeekToEnd);

    std::wstring CreationDate();
    void CreationDate(std::wstring creationDate);

    std::wstring MetaDataCreator();
    void MetaDataCreator(std::wstring metaDataCreator);
};

