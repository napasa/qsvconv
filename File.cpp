#include "pch.h"
#include "File.h"


#include <Commdlg.h>
#include <Objbase.h>
#include <Shlobj.h>
#include <Shlwapi.h>
#include <Shellapi.h>
#include <cctype>
#include <algorithm>


File& File::operator=(const File& file) {
    path = file.path;
    return *this;
}

long File::BytesSize() {
    struct _stat  buf;
    if (_wstat64i32(path.c_str(), &buf) == 0) {
        return buf.st_size;
    } else {
        return -1;
    }
}

long File::BytesSize(const std::wstring& path) {
    struct _stat  buf;
    if (_wstat64i32(path.c_str(), &buf) == 0) {
        return buf.st_size;
    } else {
        return -1;
    }
}

std::wstring File::FileTypeToString(FileType fileType) {
    switch (fileType) {
    case File::PDF:
        return L"pdf";
        break;
    case File::DOC:
        return L"doc";
        break;
    case File::DOCX:
        return L"docx";
        break;
    case File::RTF:
        return L"rtf";
        break;
    case File::XLS:
        return L"xls";
        break;
    case File::XLSX:
        return L"xlsx";
        break;
    case File::PPT:
        return L"ppt";
        break;
    case File::PPTX:
        return L"pptx";
        break;
    case File::TXT:
        return L"txt";
        break;
    case File::PNG:
        return L"png";
        break;
    case File::JPG:
        return L"jpg";
        break;
    case File::BMP:
        return L"bmp";
        break;
    case File::ICON:
        return L"ico";
        break;
    case File::GIF:
        return L"gif";
        break;
    case File::Exif:
        return L"exif";
        break;
    case File::TIFF:
        return L"tiff";
        break;
    case File::WMF:
        return L"wmf";
        break;
    case File::EMF:
        return L"emf";
        break;
    case File::HTML:
        return L"html";
        break;
    case File::DIR:
        return L"";
        break;
    case File::WEBP:
        return L"webp";
        break;
    default:
        throw std::exception("unimplemented");
        return L"";
        break;
    }
}

std::wstring File::Name() const {
    return Name(path);
}

void File::Rename(const std::wstring& name) {
    path = Directory(path) + L"\\" + name;
}

std::wstring File::Directory(const std::wstring& filename) {
    wchar_t dir[1000];
    if (filename.empty()) {
        #ifdef WIN32
        GetModuleFileName(NULL, dir, 1000);
        #endif
    } else {
        wcsncpy_s(dir, filename.c_str(), filename.size());
    }
    std::wstring temp = std::wstring(dir);
    auto count = temp.find_last_of(L'\\');
    return count != -1 ? temp.substr(0, count) : std::wstring();
}

std::wstring File::FullPath() {
    wchar_t path[1000];
    GetModuleFileName(NULL, path, 1000);
    return path;
}

void File::Path(const std::wstring& val) {
    path = val;
}

const std::wstring& File::Path() const {
    return path;
}

std::wstring File::Extension(const std::wstring& filename) {
    std::wstring::size_type extentionPos = filename.find_last_of(L'.') + 1;
    auto extension = filename.substr(extentionPos);
    std::transform(extension.begin(), extension.end(), extension.begin(), [](wchar_t c) {
        return std::tolower(c);
    });
    return extension;
}

std::wstring File::Extension()const {
    return File::Extension(path);
}

std::wstring File::TempPath() {
    WCHAR tempPath[MAX_PATH] = { 0 };
    GetTempPath(MAX_PATH, tempPath);
    return tempPath;
}

std::wstring File::GenerateUniquePath(std::wstring& originPath, bool isFile) {
    int i = 1;
    auto dir = File::Directory(originPath);
    auto basename = File::Basename(originPath);
    auto ext = File::Extension(originPath);
    while (File::Exist(originPath)) {
        if (isFile) {
            originPath = dir + L"\\" + basename + L"_" + std::to_wstring(i) + L"." + ext;
        } else {
            originPath = dir + L"\\" + basename + L"_" + std::to_wstring(i);
        }
        i++;
    }
    return originPath;
}

std::wstring File::Basename(const std::wstring& filename) {
    auto count = (filename.find_last_of(L'.') - 1) - (filename.find_last_of(L'\\') + 1) + 1;
    return filename.substr(filename.find_last_of(L'\\') + 1, count);
}

std::wstring File::Basename() const {
    return File::Basename(path);
}

std::wstring File::Name(const std::wstring& filename) {
    return filename.substr(filename.find_last_of(L'\\') + 1);
}

#ifdef WIN32
HINSTANCE File::OpenFileBySystem(const std::wstring& filename) {
    CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
    auto r = ShellExecute(NULL, NULL, filename.c_str(), NULL, NULL, SW_SHOWNORMAL);
    CoUninitialize();
    return r;
}

std::wstring File::ChooseDirectory(HWND hwnd) {
    IFileDialog* pfd;
    if (SUCCEEDED(CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&pfd)))) {
        LPWSTR szName = NULL;
        DWORD dwOptions;
        if (SUCCEEDED(pfd->GetOptions(&dwOptions))) {
            pfd->SetOptions(dwOptions | FOS_PICKFOLDERS | FOS_FORCEFILESYSTEM);
        }
        if (SUCCEEDED(pfd->Show(NULL))) {
            IShellItem* psi;
            if (SUCCEEDED(pfd->GetResult(&psi))) {
                if (!SUCCEEDED(psi->GetDisplayName(SIGDN_DESKTOPABSOLUTEPARSING, &szName))) {
                    ;
                }
                psi->Release();
            }
        }
        pfd->Release();
        return szName == NULL ? std::wstring() : szName;
    } else {
        TCHAR szDir[MAX_PATH];
        BOOL r = TRUE;
        BROWSEINFO bInfo;
        bInfo.hwndOwner = hwnd;
        bInfo.pidlRoot = NULL;
        bInfo.pszDisplayName = szDir; // Address of a buffer to receive the display name of the folder selected by the user
        bInfo.lpszTitle = L"Please, select a folder"; // Title of the dialog
        bInfo.ulFlags = 0;
        bInfo.lpfn = NULL;
        bInfo.lParam = 0;
        bInfo.iImage = -1;

        LPITEMIDLIST lpItem = SHBrowseForFolder(&bInfo);
        if (lpItem == NULL) {
            r = FALSE;
        } else {
            r = SHGetPathFromIDList(lpItem, szDir);
        }
        return r == TRUE ? szDir : std::wstring();
    }
}

bool File::OpenDirAndSelectFile(const std::wstring& filename) {
    bool r = false;
    CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
    ITEMIDLIST* pidl = ILCreateFromPath(filename.c_str());
    if (pidl) {
        if (SHOpenFolderAndSelectItems(pidl, 0, 0, 0) != S_OK) {
            r = false;
        } else {
            r = true;
        }
    } else {
        r = false;
    }
    ILFree(pidl);
    CoUninitialize();
    return r;
}

std::wstring File::SelectMultiFiles(HWND hwnd, const std::wstring& title, LPCTSTR pFilter, int filterIndex /*= 1*/) {
    std::wstring filename = L"";
    wchar_t openFileNames[200 * MAX_PATH] = { 0 };
    wchar_t directoryPath[MAX_PATH] = { 0 };

    OPENFILENAME ofn;
    ZeroMemory(&ofn, sizeof(ofn));
    ofn.lStructSize = sizeof(ofn);
    ofn.hwndOwner = hwnd;
    ofn.lpstrFile = openFileNames;
    ofn.nMaxFile = sizeof(openFileNames);
    ofn.lpstrFilter = pFilter;
    ofn.nFilterIndex = filterIndex; // 默认类型 1，2，3....
    ofn.lpstrFileTitle = const_cast<wchar_t*>(title.c_str());
    ofn.nMaxFileTitle = 0;
    ofn.lpstrInitialDir = NULL;
    ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_EXPLORER | OFN_ALLOWMULTISELECT;

    if (GetOpenFileName(&ofn) == TRUE) {
        //把第一个文件名前的复制到path,即:
        //如果只选了一个文件,就复制到最后一个'\'
        //如果选了多个文件,就复制到第一个NULL字符
        lstrcpyn(directoryPath, openFileNames, ofn.nFileOffset);
        //当只选了一个文件时,下面这个NULL字符是必需的.
        //这里不区别对待选了一个和多个文件的情况
        lstrcat(directoryPath, L"\\");
        wchar_t* pointer;
        pointer = openFileNames + ofn.nFileOffset; //把指针移到第一个文件

        while (*pointer) {
            filename.append((std::wstring(directoryPath) + pointer + L"\n"));
            pointer += lstrlen(pointer) + 1;     //移至下一个文件
        }

        return filename;
    }
    return L"";
}

bool File::CreateDeepDirectory(const std::wstring& path) {
    int m = 0;
    std::wstring tail, head;

    tail = path;
    head = tail.substr(0, 2);
    tail = tail.substr(3, tail.size());

    while (m >= 0) {
        m = tail.find(L'\\');

        head += L'\\' + tail.substr(0, m);
        if (_waccess(head.c_str(), 0) == -1) {
            _wmkdir(head.c_str());     //创建目录
        }
        tail = tail.substr(m + 1, tail.size());
    }
    return GetFileAttributes(path.c_str())&FILE_ATTRIBUTE_DIRECTORY;
}


bool File::Exist(const std::wstring& path) {
    return PathFileExists(path.c_str()) == TRUE;
}


bool File::Exist() {
    return PathFileExists(path.c_str()) == TRUE;
}

int File::CountFileInDir(const std::wstring& path) {
    int counter = 0;
    WIN32_FIND_DATA ffd;
    HANDLE hFind = INVALID_HANDLE_VALUE;

    hFind = ::FindFirstFile(path.c_str(), &ffd);
    if (hFind != INVALID_HANDLE_VALUE) {
        do { // Managed to locate and create an handle to that folder.
            if (!(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) {
                counter++;
            }
        } while (::FindNextFile(hFind, &ffd) != 0);
        ::FindClose(hFind);
    } else {
        return -1;
    }

    return counter;
}

bool File::Copy(const File& file, const std::wstring destination, bool cut) {
    BOOL res = CopyFile(file.Path().c_str(), destination.c_str(), FALSE);
    if (cut) {
        File::Delete(file);
    }
    return res == TRUE ? true : false;
}

bool File::Delete(const File& file) {
    return DeleteFile(file.Path().c_str()) == TRUE;
}


const std::wstring File::DefaultPathDir() {
    //部分XP系统下只有SHGetFolderPath, Win7以上只有SHGetKnownFolderPath
    typedef HRESULT(WINAPI * SHGetFolderPath)(HWND, int, HANDLE, DWORD, LPTSTR);
    typedef HRESULT(WINAPI * SHGetKnownFolderPath)(REFKNOWNFOLDERID, DWORD, HANDLE, PWSTR*);
    SHGetKnownFolderPath pSHGetKnownFolderPath = (SHGetKnownFolderPath)GetProcAddress(GetModuleHandle(TEXT("Shell32.dll")), "SHGetKnownFolderPath");
    SHGetFolderPath pSHGetFolderPath = (SHGetFolderPath)GetProcAddress(GetModuleHandle(TEXT("Shell32.dll")), "SHGetFolderPath");

    std::wstring appDir;
    wchar_t* path = new wchar_t[MAX_PATH];
    if (NULL != pSHGetKnownFolderPath && (S_OK == pSHGetKnownFolderPath(FOLDERID_Documents, KF_FLAG_DEFAULT_PATH, NULL, &path))) {
        appDir = path;
        CoTaskMemFree(path);
    } else if (NULL != pSHGetFolderPath && (S_OK == pSHGetFolderPath(NULL, CSIDL_COMMON_DOCUMENTS, (HANDLE) - 1, SHGFP_TYPE_DEFAULT, path))) {
        appDir = path;
        delete path;
    } else {
        appDir = std::wstring(L"C:\\");
    }
    return appDir;
}

#endif
