// Tips for Getting Started:
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
#define _CRT_SECURE_NO_WARNINGS
#pragma comment(linker,"\"/manifestdependency:type='win32' \
name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#ifndef PCH_H
#define PCH_H

// TODO: add headers that you want to pre-compile here
#define WM_GETCONFIG WM_USER+10
#define WM_PROGRESS WM_USER+11
#define WM_PAY_STATUS WM_USER+12
#define WM_INVALID_REQUEST WM_USER+13
#define WM_GETQRCODE WM_USER+13
#define TIMER_PAY_STATUS 1
#endif //PCH_H
