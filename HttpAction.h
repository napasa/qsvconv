#pragma once
#define STATIC_LIB
#include <network/Url.h>
#include <network/Router.h>
#undef STATIC_LIB
#include <json/json.h>
#pragma warning(disable : 4996)

enum RequestType {
    SOFTWARE_STRATEGY,
    GET_QRCODE,
    QUERY_STATUS
};
struct  UserData : public Http::Base {
    UserData(RequestType reqType) : requestType(reqType), stop(false) { }
    RequestType requestType;
    bool stop;
};

struct  UserPlusVoidData : public Http::Base {
    UserPlusVoidData(RequestType reqType, void* ptr) : requestType(reqType), stop(false), ptr(ptr) { }
    RequestType requestType;
    bool stop;
    void* ptr;
};

struct  WindowData : public UserData {
    WindowData(RequestType reqType, HWND hwnd, int wndMSG, int progressMSG = -1, WPARAM wparam = 0, LPARAM lparam = -1)
        : UserData(reqType), hwnd(hwnd), wndMSG(wndMSG), progressMSG(progressMSG), wparam(wparam), lparam(lparam) {}
    HWND hwnd;
    int wndMSG;
    int progressMSG;
    WPARAM wparam;
    LPARAM lparam;
    static int WN_INVALID_REQUEST;
};

#define HTTP_ACTION_INIT HttpAction::GetInstance()
#define HTTP_ACTION_OBJ HTTP_ACTION_INIT
class HttpAction : public Http::Action {
public:
    static HttpAction* GetInstance();
    virtual void Do(const Http::Task& task) override;
    virtual int Progress(double totaltime, double dltotal, double dlnow, double ultotal, double ulnow, const Http::Task& task)override;
    void GetConfig(HWND hwnd, int msg);
    void GetQrcode(HWND hwnd, int msg, std::string url);
    void QueryPayStatus(HWND hwnd, int msg, std::string url, std::string orderid);
    HttpAction();
    ~HttpAction();
public:
    static bool ParseResponse(Json::Value& value, WPARAM res, UINT uMsg, HWND hwnd, std::wstring& msg);
};

