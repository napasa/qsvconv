#ifndef UTF8_INCLUDED
#define UTF8_INCLUDED
#include <string>


class  Utf8 : public std::string {
public:
    Utf8();
    Utf8(const std::string& utf8, bool real);
    Utf8(const char* memory, int size);
    Utf8(const Utf8& utf8);
    Utf8(const std::string& ansiString);
    Utf8(const std::wstring& utf16String);
    Utf8& operator=(const Utf8& utf8);
    Utf8& operator=(const std::string& ansiString);
    Utf8& operator=(const std::wstring& utf16String);
    std::string ToAnsiString();
    std::wstring ToUtf16String();
    ~Utf8();
public:
    static std::wstring AnsiToUtf16(const std::string& ansiString);
    static Utf8 Utf16ToUtf8(const std::wstring& utf16);

    static std::wstring Utf8ToUtf16(const Utf8& utf8);
    static std::string Utf16ToAnsi(const std::wstring& utf16);
};

#endif
