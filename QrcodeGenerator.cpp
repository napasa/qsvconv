#include "pch.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <getopt.h>
#include <errno.h>
#include <string>
#include <vector>
#include <qrencode/qrencode.h>

const int bytesPerPixel = 3; /// red, green, blue
const int fileHeaderSize = 14;
const int infoHeaderSize = 40;

unsigned char* createBitmapFileHeader(int height, int width, int paddingSize) {
    int fileSize = fileHeaderSize + infoHeaderSize + (bytesPerPixel * width + paddingSize) * height;

    static unsigned char fileHeader[] = {
        0, 0, /// signature
        0, 0, 0, 0, /// image file size in bytes
        0, 0, 0, 0, /// reserved
        0, 0, 0, 0, /// start of pixel array
    };

    fileHeader[0] = (unsigned char)('B');
    fileHeader[1] = (unsigned char)('M');
    fileHeader[2] = (unsigned char)(fileSize);
    fileHeader[3] = (unsigned char)(fileSize >> 8);
    fileHeader[4] = (unsigned char)(fileSize >> 16);
    fileHeader[5] = (unsigned char)(fileSize >> 24);
    fileHeader[10] = (unsigned char)(fileHeaderSize + infoHeaderSize);

    return fileHeader;
}

unsigned char* createBitmapInfoHeader(int height, int width) {
    static unsigned char infoHeader[] = {
        0, 0, 0, 0, /// header size
        0, 0, 0, 0, /// image width
        0, 0, 0, 0, /// image height
        0, 0, /// number of color planes
        0, 0, /// bits per pixel
        0, 0, 0, 0, /// compression
        0, 0, 0, 0, /// image size
        0, 0, 0, 0, /// horizontal resolution
        0, 0, 0, 0, /// vertical resolution
        0, 0, 0, 0, /// colors in color table
        0, 0, 0, 0, /// important color count
    };

    infoHeader[0] = (unsigned char)(infoHeaderSize);
    infoHeader[4] = (unsigned char)(width);
    infoHeader[5] = (unsigned char)(width >> 8);
    infoHeader[6] = (unsigned char)(width >> 16);
    infoHeader[7] = (unsigned char)(width >> 24);
    infoHeader[8] = (unsigned char)(height);
    infoHeader[9] = (unsigned char)(height >> 8);
    infoHeader[10] = (unsigned char)(height >> 16);
    infoHeader[11] = (unsigned char)(height >> 24);
    infoHeader[12] = (unsigned char)(1);
    infoHeader[14] = (unsigned char)(bytesPerPixel * 8);

    return infoHeader;
}

void generateBitmapImage(unsigned char* image, int height, int width, const char* imageFileName) {

    unsigned char padding[3] = { 0, 0, 0 };
    int paddingSize = (4 - (width * bytesPerPixel) % 4) % 4;

    unsigned char* fileHeader = createBitmapFileHeader(height, width, paddingSize);
    unsigned char* infoHeader = createBitmapInfoHeader(height, width);

    FILE* imageFile = fopen(imageFileName, "wb");

    fwrite(fileHeader, 1, fileHeaderSize, imageFile);
    fwrite(infoHeader, 1, infoHeaderSize, imageFile);

    int i;
    for (i = 0; i < height; i++) {
        fwrite(image + (i * width * bytesPerPixel), bytesPerPixel, width, imageFile);
        fwrite(padding, 1, paddingSize, imageFile);
    }

    fclose(imageFile);
}

#define INCHES_PER_METER (100.0/2.54)

static int casesensitive = 1;
static int eightbit = 0;
static int version = 0;
static int size = 3;
static int margin = 3;
static int dpi = 72;
static int structured = 0;
static int rle = 0;
static int svg_path = 0;
static int micro = 0;
static QRecLevel level = QR_ECLEVEL_L;
static QRencodeMode hint = QR_MODE_8;
static unsigned char fg_color[4] = { 0, 0, 0, 255 };
static unsigned char bg_color[4] = { 255, 255, 255, 255 };

static int verbose = 0;

enum imageType {
    PNG_TYPE,
    PNG32_TYPE,
    EPS_TYPE,
    SVG_TYPE,
    XPM_TYPE,
    ANSI_TYPE,
    ANSI256_TYPE,
    ASCII_TYPE,
    ASCIIi_TYPE,
    UTF8_TYPE,
    ANSIUTF8_TYPE,
    UTF8i_TYPE,
    ANSIUTF8i_TYPE,
    BMP_TYPE
};

static enum imageType image_type = BMP_TYPE;

static char* optstring = "ho:r:l:s:v:m:d:t:Skci8MV";

QRcode* Encode(const unsigned char* intext, int length) {
    QRcode* code;

    if (micro) {
        if (eightbit) {
            code = QRcode_encodeDataMQR(length, intext, version, level);
        } else {
            code = QRcode_encodeStringMQR((char*)intext, version, level, hint, casesensitive);
        }
    } else if (eightbit) {
        code = QRcode_encodeData(length, intext, version, level);
    } else {
        code = QRcode_encodeString((char*)intext, version, level, hint, casesensitive);
    }

    return code;
}

void WriteBmpMargin(std::vector<std::string>& asciiQrcode, int realwidth, char* buffer, int invert) {
    int y, h;

    h = margin;

    memset(buffer, (invert ? '#' : ' '), realwidth);
    buffer[realwidth] = '\n';
    buffer[realwidth + 1] = '\0';


    for (y = 0; y < h; y++) {
        asciiQrcode.push_back(buffer);
    }
}

int WriteBmp(const QRcode* qrcode, const char* outfile, int invert) {
    std::vector<std::string> asciiQrcode;
    unsigned char* row;
    int x, y;
    int realwidth;
    char* buffer, *p;
    int buffer_s;
    char black = '#';
    char white = ' ';



    if (invert) {
        black = ' ';
        white = '#';
    }

    size = 1;

    realwidth = (qrcode->width + margin * 2) * 2;
    buffer_s = realwidth + 2;
    buffer = (char*)malloc(buffer_s);
    if (buffer == NULL) {
        fprintf(stderr, "Failed to allocate memory.\n");
        exit(EXIT_FAILURE);
    }


    /* top margin */
    WriteBmpMargin(asciiQrcode, realwidth, buffer, invert);
    /* data */
    for (y = 0; y < qrcode->width; y++) {
        row = qrcode->data + (y * qrcode->width);
        p = buffer;

        memset(p, white, margin * 2);
        p += margin * 2;

        for (x = 0; x < qrcode->width; x++) {
            if (row[x] & 0x1) {
                *p++ = black;
                *p++ = black;
            } else {
                *p++ = white;
                *p++ = white;
            }
        }

        memset(p, white, margin * 2);
        p += margin * 2;
        *p++ = '\n';
        *p++ = '\0';
        asciiQrcode.push_back(buffer);
    }

    /* bottom margin */
    WriteBmpMargin(asciiQrcode, realwidth, buffer, invert);

    free(buffer);

    unsigned char* image = (unsigned char*)malloc(asciiQrcode.size() * 2 * asciiQrcode.size() * 2 * 3);
    for (int i = 0; i < asciiQrcode.size(); i++) {
        for (int j = 0; j < asciiQrcode[0].size() - 1; j++) {
            int offset1 = (i * 2) * (asciiQrcode.size() * 2 * 3) + j * 3 + 0;
            int offset2 = (i * 2 + 1) * (asciiQrcode.size() * 2 * 3) + j * 3 + 0;

            *(image + offset1) = asciiQrcode[i][j] == ' ' ? 0xff : 0x00;
            *(image + offset1 + 1) = asciiQrcode[i][j] == ' ' ? 0xff : 0x00;
            *(image + offset1 + 2) = asciiQrcode[i][j] == ' ' ? 0xff : 0x00;

            *(image + offset2) = asciiQrcode[i][j] == ' ' ? 0xff : 0x00;
            *(image + offset2 + 1) = asciiQrcode[i][j] == ' ' ? 0xff : 0x00;
            *(image + offset2 + 2) = asciiQrcode[i][j] == ' ' ? 0xff : 0x00;
        }
    }

    generateBitmapImage((unsigned char*)image, realwidth, realwidth, outfile);
    free(image);
    return 0;
}

void Qrencode(const unsigned char* intext, int length, const char* outfile) {
    QRcode* qrcode;

    qrcode = Encode(intext, length);
    if (qrcode == NULL) {
        if (errno == ERANGE) {
            fprintf(stderr, "Failed to encode the input data: Input data too large\n");
        } else {
            perror("Failed to encode the input data");
        }
        exit(EXIT_FAILURE);
    }

    if (verbose) {
        fprintf(stderr, "File: %s, Version: %d\n", (outfile != NULL) ? outfile : "(stdout)", qrcode->version);
    }

    WriteBmp(qrcode, outfile, 0);
    QRcode_free(qrcode);
}
